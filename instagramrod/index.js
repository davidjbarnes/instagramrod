function Instagramrod() {
    var request = require("request");
    var fs = require("fs");
    var imageList = new Array();

    // Private/Internal
    function downloadImage(uri, localFileName, callback){
        request.head(uri, function(err, res, body){
            request(uri).pipe(fs.createWriteStream(localFileName)).on("close", callback);
        });
    }
    function getTotalNumberOfImages(username) {
        return new Promise((resolve, reject)=>{
            request("https://www.instagram.com/".concat(username), (error, response, body)=>{
                var totalNumberOfImages = JSON.parse(body.split("window._sharedData = ")[1].split("</script>")[0].replace(/;/g,"")).entry_data.ProfilePage[0].user.media.count;
                resolve(totalNumberOfImages);
            });
        });
    }
    function getImageList(username, totalNumberOfImages, max_id) {
        var query = max_id ? "?max_id=".concat(max_id) : "";
        var url = "https://www.instagram.com/".concat(username, "/media/", query);
    
        request(url, (error, response, body)=>{
            try {
                JSON.parse(body).items.forEach((element)=>{
                    var url = element.images.standard_resolution.url.replace("s640x640/sh0.08/","");
                    var localImageName = "./images/".concat(element.id, ".jpg");
    
                    imageList.push({
                        id:element.id,
                        url:url,
                        localImageName:localImageName
                    });
        
                    downloadImage(url, localImageName, ()=>{
                        console.log("done downloading image for".concat(username, "...", localImageName));
                    });
                }, this);
            } catch(err) {
                console.error("Error: ", err);
            }
    
            if(imageList.length < totalNumberOfImages) {
                getImageList(username, totalNumberOfImages, imageList[imageList.length-1].id);
            }
        });
    };

    // Public
    this.begin = function(username) {
        getTotalNumberOfImages(username).then((totalNumberOfImages)=>{
            getImageList(username, totalNumberOfImages, null);
        });
    };
};

module.exports = new Instagramrod();